package model.logic;

import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import model.data_structures.*;


import api.IManejadorPeliculas;

public class ManejadorPeliculas<T> implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;
	
	private ILista<VOAgnoPelicula> peliculasAgno;
	
	private ListaEncadenada<T> listaEnc;

	
	public void cargarArchivoPeliculas(String archivoPeliculas) throws IOException
	{			
		File archivo = new File(archivoPeliculas);
		
		if( archivo.exists() && archivo.toString().endsWith("movies.csv"));
		{
			try
			{
			FileReader lectorArchivo = new FileReader(archivo);
			BufferedReader lector = new BufferedReader(lectorArchivo);
			String linea = lector.readLine();
			
			
			if(linea.contains("movieId") || linea.contains("title") || linea.contains("genres"))
			{
				linea = lector.readLine();
			}
					
			while( linea != null)
			{
				
				char com = '"';
				String comillas = String.valueOf(com);
				
				String[] datosImport = linea.split(",");

				String movieIde = datosImport[0];
				String titulo = datosImport[1];					
				String generos = datosImport[2];
							
			if( titulo.startsWith(comillas))
					{
						titulo = datosImport[1]+datosImport[2];
						
						if(titulo.endsWith(comillas))
						{
							generos = datosImport[3];
						}
						
						if(titulo.endsWith(" "))
						{
							titulo = datosImport[1]+datosImport[2]+datosImport[2];
							
							generos = datosImport[4];
						}	
					}
			
			String replace = titulo.replaceAll("[()]", "@");
			String[] partir = replace.split("@");
			String tituloFinal = partir[0];

			String datosImportAnno = linea;
			
			int anno= 0;
					
			if(datosImportAnno.contains("("))
			{
				String annoreplace = datosImportAnno.replaceAll("[()]", "@");
				String[] partirDosPuntos = annoreplace.split("@");
				
				if(partirDosPuntos[1].contains("no genres listed") )
				{
					anno = 0;
				}
				
				else if(partirDosPuntos[2].contains("|") || partirDosPuntos[2].contains("Comedy") || partirDosPuntos[2].contains("Action") || partirDosPuntos[2].contains("Drama")
						|| partirDosPuntos[2].contains("Documentary") || partirDosPuntos[2].contains("Thriller") || partirDosPuntos[2].contains("Horror") 
						|| partirDosPuntos[2].contains("Children") ||  partirDosPuntos[2].contains("Western") ||  partirDosPuntos[2].contains("Romance") ||  partirDosPuntos[2].contains("Film-Noir")
						||  partirDosPuntos[2].contains("Musical") ||  partirDosPuntos[2].contains("Mystery") || partirDosPuntos[2].contains("Adventure") ||  partirDosPuntos[2].contains("Crime") 
						||  partirDosPuntos[2].contains("Sci-Fi") ||  partirDosPuntos[2].contains("War") ||  partirDosPuntos[2].contains("Animation") ||  partirDosPuntos[2].contains("no genres listed")
						||  partirDosPuntos[2].contains("Fantasy") )
				{
					anno = Integer.parseInt(partirDosPuntos[1].substring(0,4))  ;
				}
				
				else if(partirDosPuntos[3].contains("|") || partirDosPuntos[3].contains("Comedy") || partirDosPuntos[3].contains("no genres listed"))
				{
					anno = Integer.parseInt(partirDosPuntos[1]);
				}
				
				else if(partirDosPuntos[4].contains("|") || partirDosPuntos[4].contains("Comedy") || partirDosPuntos[4].contains("no genres listed"))
				{
					anno = Integer.parseInt(partirDosPuntos[3]);
				}
				
				else if(partirDosPuntos[4].contains("|") || partirDosPuntos[4].contains("Comedy") || partirDosPuntos[4].contains("no genres listed"))
				{
					anno = Integer.parseInt(partirDosPuntos[2]);
				}
				
				else
				{
					anno = 0;
				}
							
			}
					
			ILista<String> genT2 = new ListaEncadenada<>();
			genT2.agregarElementoFinal(generos);
				
			misPeliculas = new ListaEncadenada<>();		
			VOPelicula vop = new VOPelicula();
			vop.setTitulo(tituloFinal);
			vop.setAgnoPublicacion(anno);
			vop.setGenerosAsociados(genT2);			
			misPeliculas.agregarElementoFinal(vop);
			
			peliculasAgno = new ListaDobleEncadenada<>();
			VOAgnoPelicula voa = new VOAgnoPelicula();
			voa.setAgno(anno);
			voa.setPeliculas(misPeliculas);
			peliculasAgno.agregarElementoFinal(voa);
			
			
			
			/**
			 * ordenar la lista en a�os descendentes
			 */
			int in;
			for (int i = 1; i < peliculasAgno.darNumeroElementos(); i++) 
			{
				VOAgnoPelicula aux = peliculasAgno.darElemento(i);
				in = i;
				
				while (in > 0 && peliculasAgno.darElemento(in -1).getAgno() < aux.getAgno())
				{
					VOAgnoPelicula var = peliculasAgno.darElemento(in);					
					var = peliculasAgno.darElemento(in -1);
					--in;
				}					
				
				VOAgnoPelicula var2 = peliculasAgno.darElemento(in);
				var2 = aux;
		
			}
					
//			System.out.println("IDE: "+movieIde+" "+"TITLE: "+tituloFinal+" "+"GEN: "+" "+generos+" "+"A�O: "+anno);
			
			linea = lector.readLine();
								
			}	
		lector.close();
		}	
		
		catch ( IOException e )
		{
			// TODO: handle exception
			throw new IOException( "No ha sido posible cargar el CSV" );
		}			
	}
}
	// TODO Auto-generated method stub

	

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) 
	{
		ILista<VOPelicula> resp = null;
				
		for (int i = 0; i < misPeliculas.darNumeroElementos(); i++) 
		{
			String nombreAct = misPeliculas.darElemento(i).getTitulo();
			VOPelicula peliActual = misPeliculas.darElemento(i);
			
			if(busqueda.equalsIgnoreCase(nombreAct))
			{
				resp.agregarElementoFinal(peliActual);
			}
		}	
		return resp;
	}

	
	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno)
	{
		ILista<VOPelicula> pelisAgno = null;
  		
			for (int i = 0; i < misPeliculas.darNumeroElementos(); i++) 
				{
					int agnoAct = misPeliculas.darElemento(i).getAgnoPublicacion();
					
					if(agno==agnoAct)
					{
						pelisAgno.agregarElementoFinal(misPeliculas.darElemento(i));
					}
				}	
				return pelisAgno;		
		// TODO Auto-generated method stub
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() 
	{
		VOAgnoPelicula resp = null;
		VOAgnoPelicula pelActual = peliculasAgno.darElementoPosicionActual();
		
		if(pelActual !=null)
		{
			peliculasAgno.avanzarSiguientePosicion();
			resp = peliculasAgno.darElementoPosicionActual();
			
		}
		
		// TODO Auto-generated method stub
		return resp;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() 
	{
		VOAgnoPelicula resp = null;
		VOAgnoPelicula pelActual = peliculasAgno.darElementoPosicionActual();
		
		if(pelActual !=null)
		{
			peliculasAgno.retrocederPosicionAnterior();
			resp = peliculasAgno.darElementoPosicionActual();		
		}
		
		// TODO Auto-generated method stub
		return resp;

	}

}

package model.data_structures;

import java.util.Iterator;



public class ListaEncadenada<T> implements ILista<T> {

	private class NodoLista<T>{
		private T elemento;
		private int pos;
		private NodoLista<T> siguiente;

		public NodoLista(T pElem, int Ppos) {
			// TODO Auto-generated constructor stub
			elemento = pElem;
			pos = Ppos;
			siguiente = null;
		}
		public T darElemento(){
			return elemento;
		}
		public int darNum(){
			return pos;
		}
		public NodoLista<T> darSiguiente(){
			return siguiente;
		}
	}
	private class ListaIterador<T> implements Iterator<T> {
		private NodoLista<T> actual;

		public ListaIterador(NodoLista<T> primero) {
			actual = primero;
		}

		public NodoLista<T> darActual(){
			return actual;
		}

		public boolean hasNext()  {
			return actual != null;   
		}


		public T next() {
			//if (hasNext()){
			T t = actual.darElemento();
			actual = actual.darSiguiente(); 
			return t;

			//	}
			//	return null;
		}
	}

	private NodoLista<T> primero;
	private NodoLista<T> ultimo;
	private int nElem;
	@Override
	public Iterator<T> iterator() {
		ListaIterador<T> iterador = new ListaIterador<T>(primero);
		// TODO Auto-generated method stub
		return iterador;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primero==null){
			int Ppos=1;
			NodoLista<T> newUlt = new NodoLista<T>(elem, Ppos);
			primero=newUlt;
			ultimo=newUlt;
		}
		else{
			int Ppos = ultimo.pos+1;
			NodoLista<T> newUlt = new NodoLista<T>(elem, Ppos);
			ultimo.siguiente = newUlt;
			ultimo = newUlt;

		}
		nElem++;

	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> iter = iterator();
		while(iter.hasNext()){
			NodoLista<T> act = ((ListaIterador<T>) iter).darActual();
			if(act.pos==pos){
				T elem = iter.next();
				return elem;
			}
			iter.next();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return nElem;
	}

	@Override
	public T darElementoPosicionActual() {
		Iterator<T> iter = iterator();

		// TODO Auto-generated method stub
		return iter.next();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(primero==null||ultimo==null){
			return false;
		}
		else{
			NodoLista<T> rep = primero;
			primero=primero.siguiente;
			ultimo.siguiente=rep;


			// TODO Auto-generated method stub
			return true;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(primero==null||ultimo==null){
			return false;
		}
		Iterator<T> iter = iterator();
		NodoLista<T> rep = ultimo;
		while(iter.hasNext()){
			int posN = ((ListaIterador<T>) iter).darActual().pos;
			if(ultimo.pos-1==posN){
				ultimo=((ListaIterador<T>) iter).darActual();
				rep.siguiente=primero;
				primero=rep;
				return true;
			}
		}
		return false;
	}

}

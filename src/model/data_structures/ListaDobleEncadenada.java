package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private class NodoListaD<T>{

		private T elemento;
		private int pos;
		private NodoListaD<T> siguiente;
		private NodoListaD<T> anterior;
		
		public NodoListaD(T pElem, int Ppos) {
			// TODO Auto-generated constructor stub
			elemento = pElem;
			pos = Ppos;
			siguiente = null;
			anterior = null;
		}
		public T darElemento(){
			return elemento;
		}
		public int darNum(){
			return pos;
		}
		public NodoListaD<T> darSiguiente(){
			return siguiente;
		}
		public NodoListaD<T> darAnterior(){
			return anterior;
		}
	}
	private class ListaIteradorD<T> implements Iterator<T> {
		private NodoListaD<T> actual;

		public ListaIteradorD(NodoListaD<T> primero) {
			actual = primero;
		}
		
		public NodoListaD<T> darActual(){
			return actual;
		}

		public boolean hasNext()  {
			return actual != null;   
			}
	

		public T next() {
			//if (hasNext()){
				T t = actual.darElemento();
				actual = actual.darSiguiente(); 
				return t;
				
		//	}
		//	return null;
		}
	}

	
	private NodoListaD<T> primero;
	private NodoListaD<T> ultimo;
	private int nElem;
	@Override
	public Iterator<T> iterator() {
		ListaIteradorD<T> iter = new ListaIteradorD<>(primero); 
		// TODO Auto-generated method stub
		return iter;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primero==null){
			int Ppos=1;
			NodoListaD<T> newUlt = new NodoListaD<T>(elem, Ppos);
			primero=newUlt;
			ultimo=newUlt;
		}
		else{
			int Ppos = ultimo.pos+1;
			NodoListaD<T> newUlt = new NodoListaD<T>(elem, Ppos);
			NodoListaD<T> act = ultimo;
			ultimo.siguiente = newUlt;
			newUlt.anterior = act;
			ultimo = newUlt;
			
		}
		nElem++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> iter = iterator();
		while(iter.hasNext()){
			NodoListaD<T> act = ((ListaIteradorD<T>) iter).darActual();
			if(act.pos==pos){
				T elem = (T) iter.next();
				return elem;
			}
			iter.next();
			
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return nElem;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return primero.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(primero==null||ultimo==null){
			return false;
		}
		else{
			NodoListaD<T> rep = primero;
			primero=primero.siguiente;
			ultimo.siguiente=rep;
			rep.anterior=ultimo;
			
			// TODO Auto-generated method stub
			return true;
			
		}
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
if(primero==null||ultimo==null){
	return false;
}
		NodoListaD<T> rep = ultimo;
		ultimo = ultimo.anterior;
		rep.siguiente=primero;
		primero.anterior=rep;
		primero=rep;
		
		// TODO Auto-generated method stub
		return true;
	}

}
